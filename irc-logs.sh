#!/bin/bash

LOGDIR="/home/znc/.znc/users/EspadaV8/moddata/log/"
HTMLDIR="/srv/http/logger/"
BACKUP="/home/znc/logger-backups/"
DATE=`date "+%Y-%m-%d-%H.%M.%S"`

logs2html  -g \*chili\*.log "$LOGDIR"

tar cjf "$BACKUP$DATE.tbz2" "$HTMLDIR"
mv "$LOGDIR"*.{html,css} "$HTMLDIR"
