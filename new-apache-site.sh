#!/bin/bash

echo "Please enter the new domain name (do not include the www) []"
read DOMAINNAME

if [ "z" = "z$DOMAINNAME" ];
then
    echo "No domain given, exiting"
    exit 1
fi

USERNAME=`echo "$DOMAINNAME"|cut -f1 -d.`

echo "Please enter the new user for the domain [$USERNAME]"
read NEWUSER

if [ "z" != "z$NEWUSER" ];
then
    USERNAME="$NEWUSER"
fi

(
cat <<EOF
<VirtualHost 127.0.0.1:8001>
    ServerAdmin  itsupport@bcm.com.au
    ServerName                            $DOMAINNAME
    ServerAlias                       www.$DOMAINNAME
    DocumentRoot                 /srv/www/$DOMAINNAME/public_html/
    ErrorLog                     /srv/www/$DOMAINNAME/logs/error.log
    CustomLog                    /srv/www/$DOMAINNAME/logs/access.log combined
    php_admin_value error_log    /srv/www/$DOMAINNAME/logs/php.error.log

    <IfModule mpm_itk_module>
        AssignUserId $USERNAME $USERNAME
    </IfModule>

    <IfModule mod_expires.c>
        ExpiresActive On
        ExpiresByType application/vnd.ms-fontobject "access plus 1 year"
        ExpiresByType application/x-font-ttf "access plus 1 year"
        ExpiresByType application/x-font-opentype "access plus 1 year"
        ExpiresByType application/x-font-woff "access plus 1 year"
        ExpiresByType image/svg+xml "access plus 1 year"
        ExpiresByType font/ttf "access plus 1 year"
        ExpiresByType font/otf "access plus 1 year"
        ExpiresByType font/x-woff "access plus 1 year"
        ExpiresByType application/font-woff "access plus 1 year"
        ExpiresByType font/opentype "access plus 1 year"
        ExpiresByType video/webm "access plus 1 year"
        ExpiresByType video/ogg "access plus 1 year"
        ExpiresByType video/mp4 "access plus 1 year"

        ExpiresByType image/gif "access plus 1 months"
        ExpiresByType image/jpg "access plus 1 months"
        ExpiresByType image/jpeg "access plus 1 months"
        ExpiresByType image/png "access plus 1 months"
        ExpiresByType image/vnd.microsoft.icon "access plus 1 months"
        ExpiresByType image/x-icon "access plus 1 months"
        ExpiresByType image/ico "access plus 1 months"
        ExpiresByType application/javascript "now plus 1 months"
        ExpiresByType application/x-javascript "now plus 1 months"
        ExpiresByType text/javascript "now plus 1 months"
        ExpiresByType text/css "now plus 1 months"
        ExpiresDefault "access plus 1 days"
    </IfModule>

  # Audio
    AddType audio/mp4                                   f4a f4b m4a
    AddType audio/ogg                                   oga ogg opus

  # Data interchange
    AddType application/json                            json map topojson
    AddType application/ld+json                         jsonld
    AddType application/vnd.geo+json                    geojson

  # JavaScript
    # Normalize to standard type.
    # http://tools.ietf.org/html/rfc4329#section-7.2
    AddType application/javascript                      js

  # Manifest files

    # If you are providing a web application manifest file (see the
    # specification: http://w3c.github.io/manifest/), it is recommended
    # that you serve it with the 'application/manifest+json' media type.
    #
    # Because the web application manifest file doesn't have its own
    # unique file extension, you can set its media type either by matching:
    #
    # 1) the exact location of the file (this can be done using a directive
    #    such as '<Location>', but it will NOT work in the '.htaccess' file,
    #    so you will have to do it in the main server configuration file or
    #    inside of a '<VirtualHost>' container)
    #
    #    e.g.:
    #
    #       <Location "/.well-known/manifest.json">
    #           AddType application/manifest+json               json
    #       </Location>
    #
    # 2) the filename (this can be problematic as you will need to ensure
    #    that you don't have any other file with the same name as the one
    #    you gave to your web application manifest file)
    #
    #    e.g.:
    #
    #       <Files "manifest.json">
    #           AddType application/manifest+json               json
    #       </Files>

    AddType application/x-web-app-manifest+json         webapp
    AddType text/cache-manifest                         appcache manifest

  # Video
    AddType video/mp4                                   f4v f4p m4v mp4
    AddType video/ogg                                   ogv
    AddType video/webm                                  webm
    AddType video/x-flv                                 flv

  # Web fonts
    AddType application/font-woff                       woff
    AddType application/font-woff2                      woff2
    AddType application/vnd.ms-fontobject               eot

    # Browsers usually ignore the font media types and simply sniff
    # the bytes to figure out the font type.
    # http://mimesniff.spec.whatwg.org/#matching-a-font-type-pattern

    # Chrome however, shows a warning if any other media types are used
    # for the following two font types.

    AddType application/x-font-ttf                      ttc ttf
    AddType font/opentype                               otf

    AddType image/svg+xml                               svg svgz

  # Other
    AddType application/octet-stream                    safariextz
    AddType application/x-chrome-extension              crx
    AddType application/x-opera-extension               oex
    AddType application/x-xpinstall                     xpi
    AddType application/xml                             atom rdf rss xml
    AddType image/webp                                  webp
    AddType image/x-icon                                cur ico
    AddType text/vtt                                    vtt
    AddType text/x-component                            htc
    AddType text/x-vcard                                vcf
</VirtualHost>
EOF
) > "/etc/apache2/sites-available/$DOMAINNAME"

mkdir -p "/srv/www/$DOMAINNAME/public_html"
mkdir -p "/srv/www/$DOMAINNAME/logs"

useradd -s /bin/zsh -U -m "$USERNAME"
passwd -d "$USERNAME"

ln -s "/srv/www/$DOMAINNAME/public_html" "/home/$USERNAME/public_html"
ln -s "/srv/www/$DOMAINNAME/logs" "/home/$USERNAME/logs"

touch "/srv/www/$DOMAINNAME/logs/php.error.log"

chown "$USERNAME:$USERNAME" "/home/$USERNAME/public_html"
chown "$USERNAME:$USERNAME" "/srv/www/$DOMAINNAME/logs"

a2ensite "$DOMAINNAME"
service apache2 reload

(
cat <<EOF
#server {
#  server_name www.$DOMAINNAME;
#  return 301 http://$DOMAINNAME/\$request_uri permanent;
#  expires max;
#}

server {
  listen 80;
  server_name $DOMAINNAME;

  client_max_body_size 10M;

  charset utf-8;

  location / {
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header Host \$http_host;
    proxy_set_header X-NginX-Proxy true;

    proxy_pass http://127.0.0.1:8001;
    proxy_redirect off;
    proxy_buffering off;
  }
}

#server {
#  listen 443 ssl;
#
#  server_name www.$DOMAINNAME;
#
#  client_max_body_size 10M;
#
#  charset utf-8;
#
#  ssl_certificate         /etc/ssl/certs/cert.pem;
#  ssl_certificate_key     /etc/ssl/private/cert.key;
#
#  ssl_dhparam /etc/nginx/dh2048.pem;
#
#  ssl_session_timeout 5m;
#  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
#  ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
#  ssl_prefer_server_ciphers on;
#  ssl_session_cache shared:SSL:50m;
#
#  ssl_stapling on;
#  ssl_stapling_verify on;
#  ssl_trusted_certificate /etc/ssl/certs/cert.pem;
#  resolver 8.8.8.8 8.8.4.4;
#
#  return 301 http://$DOMAINNAME/\$request_uri permanent;
#  expires max;
#}

#server {
#  listen 443 ssl;
#
#  server_name $DOMAINNAME;
#
#  client_max_body_size 10M;
#
#  charset utf-8;
#
#  ssl_certificate         /etc/ssl/certs/cert.pem;
#  ssl_certificate_key     /etc/ssl/private/cert.key;
#
#  ssl_dhparam /etc/nginx/dh2048.pem;
#
#  ssl_session_timeout 5m;
#  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
#  ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
#  ssl_prefer_server_ciphers on;
#  ssl_session_cache shared:SSL:50m;
#
#  ssl_stapling on;
#  ssl_stapling_verify on;
#  ssl_trusted_certificate /etc/ssl/certs/cert.pem;
#  resolver 8.8.8.8 8.8.4.4;
#
#  location / {
#    proxy_set_header X-Real-IP \$remote_addr;
#    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
#    proxy_set_header Host \$http_host;
#    proxy_set_header X-NginX-Proxy true;
#    proxy_set_header X-Forwarded-Protocol \$scheme;
#
#    proxy_pass http://127.0.0.1:8001;
#    proxy_redirect off;
#    proxy_buffering off;
#  }
#}
EOF
) > "/etc/nginx/sites-available/$DOMAINNAME"

ngxensite "$DOMAINNAME"
service nginx reload
