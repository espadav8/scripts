#!/bin/bash

mailto="espadav8@gmail.com"

# Download only; package files are only retrieved, not 
# unpacked or installed.
apt-get -dqq update
apt-get -dyqq upgrade

subject="Updates for $(hostname) on $(date +%Y-%m-%d)" 

has_upgrades=$(apt-get -s upgrade | grep ^Inst)
if [ "z$has_upgrades" != "z" ] ; then
    for email in "$mailto"; do
        echo "$has_upgrades" | mail -s "$subject" "$email" 
    done
fi
