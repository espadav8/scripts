#!/bin/sh

/root/Workspace/letsencrypt/letsencrypt-auto certonly --webroot \
    -w /srv/www/ghost.espadav8.co.uk/public_html/ -d espadav8.co.uk -d www.espadav8.co.uk \
    --renew-by-default

service nginx reload
