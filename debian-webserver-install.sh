#!/bin/bash
QUIET="-q=2"
REDIRECTION=" > /dev/null"

echo "Is this a local install [Y|n]?"
read LOCAL
if [ -z "$LOCAL" ]
then
  LOCAL="y"
fi

LOCAL=`echo "$LOCAL" | tr '[A-Z]' '[a-z]'`

if [ "zy" != "z$LOCAL" ]
then
  echo "Please enter the hostname [stark]"
  read HOSTNAME

  if [ -z "$HOSTNAME" ]
  then
      HOSTNAME="stark"
  fi

  echo "Please your username [asmith]"
  read USERNAME

  if [ -z "$USERNAME" ]
  then
      USERNAME="asmith"
  fi

  echo "Please enter a port for SSH [random]"
  read SSHPORT
  if [ -z "$SSHPORT" ]
  then
    SSHPORT=`shuf -i 2500-2600 -n 1`
    echo '----'
    echo "Your random SSH port is - $SSHPORT - make a note or you won't be able to connect"
    echo '----'
  fi

  echo "Please enter the Public IP [192.168.1.137]:"
  read PUBLICIP

  if [ -z "$PUBLICIP" ]
  then
    PUBLICIP="192.168.1.137"
  fi

  echo "Please enter the Public NetMask [255.255.255.0]:"
  read PUBLICNETMASK

  if [ -z "$PUBLICNETMASK" ]
  then
    PUBLICNETMASK="255.255.255.0"
  fi

  echo "Please enter the default gateway [192.168.1.1]:"
  read GATEWAY

  if [ -z "$GATEWAY" ]
  then
    GATEWAY="192.168.1.1"
  fi

  echo "Please enter the Private IP []:"
  read PRIVATEIP

  if [ "z" != "z$PRIVATEIP" ]
  then
    echo "Please enter the Private NetMask [255.255.128.0]:"
    read PRIVATENETMASK

    if [ -z "$PRIVATENETMASK" ]
    then
      PRIVATENETMASK="255.255.128.0"
    fi
  fi

  echo "Please enter your UNIX timezone [Japan]"
  read UNIXTIMEZONE
  if [ "z" = "z$UNIXTIMEZONE" ]
  then
      UNIXTIMEZONE="Japan"
  fi
else
  QUIET=""
  REDIRECTION=""
  SSHPORT="22"

  echo "Please the username you used during installation [asmith]"
  read USERNAME

  if [ -z "$USERNAME" ]
  then
      USERNAME="asmith"
  fi
fi

echo "Please copy your PUBLIC SSH key"
read PUBLICSSHKEY

echo "Please enter your PHP timezone [Asia/Tokyo]"
read PHPTIMEZONE
if [ "z" = "z$PHPTIMEZONE" ]
then
    PHPTIMEZONE="Asia/Tokyo"
fi

CODENAME=`lsb_release -c -s`
echo "Please enter the debian release name [$CODENAME]"
read OVERRIDE_CODENAME
if [ "z" != "z$OVERRIDE_CODENAME" ]
then
    CODENAME="$OVERRIDE_CODENAME"
fi

if [ "zy" != "z$LOCAL" ]
then
  echo "Setting hostname"
  hostname "$HOSTNAME"
  echo "$HOSTNAME" > /etc/hostname
  echo "$PUBLICIP $HOSTNAME.bcm.com.au $HOSTNAME" >> /etc/hosts

  echo "Updating network interfaces"
  mv /etc/network/interfaces /etc/network/interfaces.bak
  (
  cat <<EOF
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug eth0
auto eth0 #eth0:0

iface eth0 inet static
 address $PUBLICIP
 netmask $PUBLICNETMASK
 gateway $GATEWAY
EOF
  ) > /etc/network/interfaces

  if [ "z" != "z$PRIVATEIP" ]
  then
      echo "Adding private interface"
  (
  cat <<EOF

# Private network IP
iface eth0:0 inet static
 address $PRIVATEIP
 netmask $PRIVATENETMASK
EOF
) >> /etc/network/interfaces

      echo "Adding default DROP rule for private IP"
      iptables -I INPUT -i eth0 -j DROP -d "$PRIVATEIP"
  fi

  echo "Setting localtime"
  ln -sf "/usr/share/zoneinfo/$UNIXTIMEZONE" /etc/localtime
fi

echo "Adding dotdeb repo"
echo deb http://packages.dotdeb.org "$CODENAME" all > /etc/apt/sources.list.d/dotdeb.list
echo deb-src http://packages.dotdeb.org "$CODENAME" all >> /etc/apt/sources.list.d/dotdeb.list
wget http://www.dotdeb.org/dotdeb.gpg -O -|apt-key add -

echo "Updating apt database"
aptitude $QUIET update $REDIRECTION

echo "Updating system"
aptitude $QUIET -y safe-upgrade $REDIRECTION

echo "Installing default packages"
aptitude $QUIET -y install sudo zsh tmux git subversion vim htop bsd-mailx $REDIRECTION

if [ "zy" != "z$LOCAL" ]
then
  echo "Adding new user"
  useradd -U -G sudo -m "$USERNAME"
  passwd "$USERNAME"
else
  echo "Updating user account"
  usermod -a -G sudo "$USERNAME"
fi
chsh -s /bin/zsh "$USERNAME"

mkdir -p "/home/$USERNAME/.ssh"
touch "/home/$USERNAME/.ssh/authorized_keys"
if [ "z" != "z$PUBLICSSHKEY" ]
then
  echo "Copying public SSH key"
  echo "$PUBLICSSHKEY" >> "/home/$USERNAME/.ssh/authorized_keys"
fi
chown -R "$USERNAME:$USERNAME" "/home/$USERNAME/.ssh"
chmod 0700 "/home/$USERNAME/.ssh"
chmod 0600 "/home/$USERNAME/.ssh/authorized_keys"

echo "Locking down SSH a bit"
sed -i -e 's/#\?PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
sed -i -e 's/#\?ChallengeResponseAuthentication yes/ChallengeResponseAuthentication no/g' /etc/ssh/sshd_config
sed -i -e 's/#\?UsePAM yes/UsePAM no/g' /etc/ssh/sshd_config
sed -i -e 's/#\?RSAAuthentication no/RSAAuthentication yes/g' /etc/ssh/sshd_config
sed -i -e 's/#\?PubkeyAuthentication no/PubkeyAuthentication yes/g' /etc/ssh/sshd_config
sed -i -e 's/Port 22/Port '"$SSHPORT"'/g' /etc/ssh/sshd_config

echo "Restarting SSH. If you get disconnected login using your new user and SSH key."
service ssh restart

echo "Installing MySQL"
aptitude $QUIET -y install mysql-server $REDIRECTION

echo "Installing PHP and modules"
aptitude $QUIET -y install php5-common php5-cli php5-suhosin php5-apc php-pear php5-gd php5-memcache php5-mcrypt php5-gmp php5-mysql php5-fpm php5-curl $REDIRECTION

echo "Creating PHP-FPM folders"
mkdir -p /var/run/php5-fpm
chmod -R 0755 /var/run/php5-fpm

echo "Installing Apache2 and modules"
aptitude $QUIET -y install apache2 apache2-doc apache2-utils apache2-mpm-itk libapache2-mod-php5 libapache2-mod-rpaf $REDIRECTION

echo "Stopping apache service"
service apache2 stop

echo "Enabling extra modules"
a2enmod deflate expires rewrite rpaf

echo "Disabling default site"
a2dissite default

echo "Updating configuration"
sed -i -e 's/ServerSignature On/ServerSignature Off/g' /etc/apache2/conf.d/security
sed -i -e 's/ServerTokens OS/ServerTokens Prod/g' /etc/apache2/conf.d/security
sed -i -e 's/NameVirtualHost \*:80/NameVirtualHost 127.0.0.1:8001/g' /etc/apache2/ports.conf
sed -i -e 's/Listen 80/Listen 127.0.0.1:8001/g' /etc/apache2/ports.conf

echo "Installing nginx"
aptitude $QUIET -y install nginx $REDIRECTION

echo "Stopping nginx"
service nginx stop

echo "Disabling default nginx site"
ngxdissite default

echo "Creating blackhole nginx site"
(
cat <<EOF
server {
    server_name _;
    access_log off;

    return 204;
}
EOF
) > /etc/nginx/sites-available/00_blackhole

echo "Enabling blackhole site"
ngxensite 00_blackhole

echo "Start nginx"
service nginx start

echo "Updating PHP timezones"
sed -i -e 's#;date.timezone =#date.timezone = '\'"$PHPTIMEZONE"\''#g' /etc/php5/apache2/php.ini
sed -i -e 's#;date.timezone =#date.timezone = '\'"$PHPTIMEZONE"\''#g' /etc/php5/cli/php.ini
sed -i -e 's#;date.timezone =#date.timezone = '\'"$PHPTIMEZONE"\''#g' /etc/php5/fpm/php.ini

echo "Manual steps coming up..."
echo "Secure MySQL"
mysql_secure_installation

echo "Installing postfix"
aptitude install postfix
dpkg-reconfigure postfix

echo "Sending a test email"
SSHPUBRSA=`ssh-keygen -lvf /etc/ssh/ssh_host_rsa_key.pub`
SSHPUBDSA=`ssh-keygen -lvf /etc/ssh/ssh_host_dsa_key.pub`
echo -e "RSA Public Key\n\n$SSHPUBRSA\n\n\n\nDSA Public Key\n\n$SSHPUBDSA" | mail -s "Hi there, this seems to be working." "$USERNAME@bcm.com.au"

echo "Improving root"
mkdir /root/bin
wget https://raw.github.com/EspadaV8/scripts/master/new-apache-site.sh -O /root/bin/new-apache-site
chmod +x /root/bin/new-apache-site
echo "export PATH=\"/root/bin:\$PATH\"" >> /root/.profile

echo "Adding authorized_keys to skel"
mkdir /etc/skel/.ssh
touch /etc/skel/.ssh/authorized_keys
chmod 0755 /etc/skel/.ssh
chmod 0600 /etc/skel/.ssh/authorized_keys

echo "All done. Enjoy your new server!"
