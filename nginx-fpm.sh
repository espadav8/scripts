#!/bin/bash

if [ "z" = "x$1" ];
then
    echo "Please enter the new domain name (do not include the www) []"
    read DOMAINNAME

    if [ "z" = "z$DOMAINNAME" ];
    then
        echo "No domain given, exiting"
        exit 1
    fi
else
    DOMAINNAME="$1"
fi

if [ "z" = "z$2" ];
then
    USERNAME=`echo "$DOMAINNAME"|cut -f1 -d.`

    echo "Please enter the new user for the domain [$USERNAME]"
    read NEWUSER

    if [ "z" != "z$NEWUSER" ];
    then
        USERNAME="$NEWUSER"
    fi
else
    USERNAME="$2"
fi

mkdir -p "/srv/www/$DOMAINNAME/public_html"
mkdir -p "/srv/www/$DOMAINNAME/logs"

useradd -s /bin/zsh -U -m "$USERNAME"
passwd -d "$USERNAME"

ln -s "/srv/www/$DOMAINNAME/public_html" "/home/$USERNAME/public_html"
ln -s "/srv/www/$DOMAINNAME/logs" "/home/$USERNAME/logs"

touch "/srv/www/$DOMAINNAME/logs/php.error.log"

chown "$USERNAME:$USERNAME" "/home/$USERNAME/public_html"
chown "$USERNAME:$USERNAME" "/srv/www/$DOMAINNAME/logs"

(
cat <<EOF
[$USERNAME-1]
listen = /var/run/php5-fpm/$DOMAINNAME-1.sock
user = $USERNAME
group = $USERNAME
slowlog = /srv/www/$DOMAINNAME/logs/fpm-slowlog-1.log
pm = dynamic
pm.max_children = 20
pm.start_servers = 5
pm.min_spare_servers = 5
pm.max_spare_servers = 10
pm.max_requests = 1000
php_flag[display_errors] = off
php_flag[display_startup_errors] = off
php_admin_flag[log_errors] = on
EOF
) > /etc/php5/fpm/pool.d/"$DOMAINNAME".conf

service php5-fpm restart

(
cat <<EOF
upstream $USERNAME-php {
    server unix:/var/run/php5-fpm/$DOMAINNAME-1.sock;
}

#server {
#    server_name www.$DOMAINNAME;
#    rewrite ^/(.*) http://$DOMAINNAME/$1 permanent;
#    expires max;
#}

server {
    server_name $DOMAINNAME;
    set \$upstreamfpm '$USERNAME-php';

    root /srv/www/$DOMAINNAME/public_html;

    access_log /srv/www/$DOMAINNAME/logs/access.log;
    error_log  /srv/www/$DOMAINNAME/logs/error.log;

    index index.php;

    include global/gzip.conf;
    include global/restrictions.conf;
    include global/wordpress.conf;
}
EOF
) > "/etc/nginx/sites-available/$DOMAINNAME"

ngxensite "$DOMAINNAME"
service nginx reload
