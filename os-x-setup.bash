#!/bin/bash

function join { local IFS="$1"; shift; echo "$*"; }

PACKAGES=(
    "codeception"
    "composer"
    "git"
    "git-flow-avh"
    "node"
    "phantomjs"
    "php-code-sniffer"
    "php-cs-fixer"
    "php70"
    "php70-pdo-pgsql"
    "phpmd"
    "phpunit"
    "postgresql"
    "wget"
)
PACKAGES=$( join ' ' "${PACKAGES[@]}" )

OPTIONAL_PACKAGES=(
    "hub"
    "thefuck"
    "tig"
    "tmux"
    "zsh"
)
OPTIONAL_PACKAGES=$( join ' ' "${OPTIONAL_PACKAGES[@]}" )

VIRTUALBOX="http://download.virtualbox.org/virtualbox/5.0.8/VirtualBox-5.0.8-103449-OSX.dmg"
VIRTUALBOX_EXTENSIONS="http://download.virtualbox.org/virtualbox/5.0.8/Oracle_VM_VirtualBox_Extension_Pack-5.0.8-103449.vbox-extpack"
VAGRANT="https://dl.bintray.com/mitchellh/vagrant/vagrant_1.7.4.dmg"

SSH_KEY=~/.ssh/test

WORKSPACE="$HOME/Workspace"

echo "Please make sure that you are up to date with OS X and have Xcode installed"
echo "along with the Xcode commmand line tools"
echo
echo "Press enter to continue"
read

echo "Installing Homebrew"
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

echo "Tapping required homebrew kegs"
echo "Tapping homebrew/dupes"
brew tap homebrew/dupes

echo "Tapping homebrew/versions"
brew tap homebrew/versions

echo "Tapping homebrew/homebrew-php"
brew tap homebrew/homebrew-php

echo "Installing required homebrew packages"
echo
echo "Installing - $PACKAGES"
brew install $PACKAGES

echo "Installing Virtualbox and Vagrant"
wget "$VIRTUALBOX" -O /tmp/virtualbox.dmg
wget "$VIRTUALBOX_EXTENSIONS" -O /tmp/Oracle_VM_VirtualBox_Extension_Pack-5.0.8-103449.vbox-extpack
wget "$VAGRANT" -O /tmp/vagrant.dmg

sudo hdiutil attach /tmp/virtualbox.dmg
sudo installer -package /Volumes/VirtualBox/VirtualBox.pkg -target /
sudo hdiutil detach /Volumes/VirtualBox

sudo hdiutil attach /tmp/vagrant.dmg
sudo installer -package /Volumes/Vagrant/Vagrant.pkg -target /
sudo hdiutil detach /Volumes/Vagrant

open /tmp/Oracle_VM_VirtualBox_Extension_Pack-5.0.8-103449.vbox-extpack

if [ ! -e "$SSH_KEY" ]
then
    echo "Creating SSH key (without password)"
    ssh-keygen -f $SSH_KEY -N '' -t rsa
    cat "$SSH_KEY.pub" | pbcopy
    echo
    echo "Please paste in the contents of the clipboard as a new key on GitHub"
else
    echo
    echo "Please make sure that you add the contents of your SSH key public file as a key on GitHub"
fi
echo "If the key hasn't been added to GitHub then you will be unable to checkout the repositories"
echo
echo "Press enter to continue after the key has been added"
read

echo "Creating Workspace folders"
mkdir -p "$WORKSPACE/"{homestead,lapis}

echo "Cloning homestead-php7"
git clone -b php-7 https://github.com/laravel/homestead.git "$WORKSPACE/homestead"
bash "$WORKSPACE/homestead/init.sh"

echo
echo "Cloning lapis"
git clone git@github.com:intellihr/lapis.git "$WORKSPACE/lapis"
cd "$WORKSPACE/lapis"
git flow init -d

cat > "$WORKSPACE/lapis/.git/hooks/pre-commit" << EOF
#!/bin/bash
if git rev-parse --verify HEAD >/dev/null 2>&1
then
    against=HEAD
else
    # Initial commit: diff against an empty tree object
    against=4b825dc642cb6eb9a060e54bf8d69288fbee4904
fi

exec git diff-index --check --cached $against --
EOF
chmod +x "$WORKSPACE/lapis/.git/hooks/pre-commit"

echo
echo "Updating homestead.yaml file with needed options"
sed -i '' -e 's/Code/Workspace/g' ~/.homestead/Homestead.yaml
sed -i '' -e 's/homestead.app/lapis.dev/' ~/.homestead/Homestead.yaml
sed -i '' -e 's/Workspace\/Laravel/Workspace\/lapis/' ~/.homestead/Homestead.yaml

cat >> ~/.homestead/Homestead.yaml << EOF

networks:
    - type: public_network
      ip: "10.1.1.XXX"
      bridge: "en0: Ethernet"
EOF

echo "Please edit homestead.yaml and set your static network IP and hostname"
open ~/.homestead/Homestead.yaml
echo "Please press enter when you have finished"

echo "Main packages have been installed and configured"
echo "Would you like to install optional packages to improve your command line? [Y/n]"
read INSTALL_OPTIONAL

if [ -z "$INSTALL_OPTIONAL" ]
then
    INSTALL_OPTIONAL="y"
fi
INSTALL_OPTIONAL=`echo "$INSTALL_OPTIONAL" | tr '[A-Z]' '[a-z]'`

if [ "zy" = "z$INSTALL_OPTIONAL" ]
then
    echo "Installing optional packages"
    echo $OPTIONAL_PACKAGES
    brew install $OPTIONAL_PACKAGES

    echo "Installing oh my zsh"
    sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

    sed -i '' -e 's/^plugins=.*/plugins=(git svn history-substring-search vagrant brew tmux grunt git-flow-avh zsh-syntax-highlighting laravel5 thefuck)/' ~/.zshrc

    echo
    echo "Adding useful aliases"

    cat >> ~/.zshrc << EOF
alias gs="git stash"
alias gsp="git stash pop"
alias gr="git rebase -ip"
alias gffs="git flow feature start"
alias gfff="git flow feature finish"

alias da="composer dump-autoload"
alias art="php artisan"
alias artisan="php artisan"
alias migrate="php artisan migrate"
alias tinker="php artisan tinker"
alias routes="php artisan route:list"
alias rollback="php artisan migrate:rollback"
alias g:m="php artisan generate:migration"

alias uuid="echo 'import uuid, sys\ncount=1\nif len(sys.argv) > 1:\n  count=int(sys.argv[1], 10)\nfor i in range(0, count):\n  print uuid.uuid4()'|python -"

TF_ALIAS=fuck alias fuck='eval $(thefuck $(fc -ln -1 | tail -n 1)); fc -R'

EOF

    echo
    echo "Optional extras installed and configured. I recommend you go and check out the oh-my-zsh"
    echo "themes and set it in your ~/.zshrc file (https://github.com/robbyrussell/oh-my-zsh/wiki/themes)"
    echo
fi

echo "All done!"
